<?php

namespace Drupal\agorabase\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Provides the current day cache context.
 *
 * Cache context ID: 'current_day'.
 */
class CurrentDayCacheContext implements CacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Current day');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $date = new DrupalDateTime();

    return $date->format('Ymd');
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
