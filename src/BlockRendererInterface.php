<?php

namespace Drupal\agorabase;

/**
 * Defines the block renderer interface.
 */
interface BlockRendererInterface {

  /**
   * Utility function to render a given block by its plugin ID.
   *
   * This function loads the block from the block plugin manager, doing all
   * necessary checks for existence and permission, and finally class the build
   * function on the block, taking care of adding the cache metadata as well.
   *
   * @param string $block_plugin_id
   *   The block's plugin ID.
   * @param array $block_config
   *   The block configuration. Defaults to an empty array.
   *
   * @return array
   *   A render array. Can be empty, if block does not exist, the current user
   *   has not the permission to view, or the block itself returns an empty
   *   build.
   */
  public function renderBlock(string $block_plugin_id, array $block_config = []): array;

}
