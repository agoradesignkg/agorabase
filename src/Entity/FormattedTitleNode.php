<?php

namespace Drupal\agorabase\Entity;

use Drupal\node\Entity\Node;

/**
 * Extends and swaps Drupal's Node entity class.
 */
class FormattedTitleNode extends Node {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    $title = parent::getTitle();

    $filters = agorabase_get_markdown_filter_instances();
    foreach ($filters as $filter) {
      $title = $filter->removePseudoMarkup($title);
    }
    return $title;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getTitle();
  }

}
