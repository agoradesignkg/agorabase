<?php

namespace Drupal\agorabase\TwigExtension;

use Drupal\Core\Render\Markup;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Defines the strip comments Twig extension.
 *
 * @todo deprecated in D10 (remove_html_comments).
 */
class StripComments extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new TwigFilter('strip_comments', [$this, 'doStripComments']),
    ];
  }

  /**
   * Renders the given argument, removes HTML comments and new lines.
   *
   * @param string $string
   *   Must be a string argument in order to work. So it should be used in a
   *   chained call stack only, where the render command has already been
   *   executed. Any other type than string will just be returned as is.
   *
   * @return string
   *   A string without any HTML comments.
   */
  public function doStripComments($string) {
    if (empty($string)) {
      return $string;
    }
    return preg_replace('/<!--(.|\s)*?-->\s*|\r|\n/', '', $string);
  }

}
