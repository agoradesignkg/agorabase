<?php

namespace Drupal\agorabase\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig Extension for inline SVG.
 */
class SvgInline extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('icon', [$this, 'getInlineSvg']),
    ];
  }

  /**
   * Callback for the icon() Twig function.
   *
   * @param string $name
   *   The name (ID) of the icon.
   * @param string $title
   *   The title to set.
   * @param bool $wrap
   *   Whether to wrap the svg inside a wrapper span element. Defaults to FALSE.
   *
   * @return array
   *   The inline SVG render array.
   */
  public static function getInlineSvg(string $name, string $title = '', bool $wrap = FALSE): array {
    return [
      '#theme' => 'svg_inline',
      '#name' => $name,
      '#title' => $title,
      '#wrap' => $wrap,
    ];
  }

}
