<?php

namespace Drupal\agorabase\Util;

use Dotenv\Dotenv;

/**
 * Helper class for dealing with our .install.env file.
 */
class InstallationVariables {

  /**
   * Load installation environment variables from .install.env file.
   *
   * @return array
   *   The installation variables.
   */
  public static function loadInstallationVariables(): array {
    // Load installation environment variables.
    $app_root = \Drupal::hasService('kernel') ? \Drupal::root() : DRUPAL_ROOT;
    if (str_ends_with($app_root, '/web')) {
      $app_root = substr($app_root, 0, -4);
    }
    $dotenv = Dotenv::createArrayBacked($app_root, ['.install.env'], TRUE, 'UTF-8');
    return $dotenv->load();
  }

}
