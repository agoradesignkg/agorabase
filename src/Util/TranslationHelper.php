<?php

namespace Drupal\agorabase\Util;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\Entity\ContentLanguageSettings;
use Drupal\locale\SourceString;

/**
 * Helper functions for making entity types translatable and similar stuff.
 */
class TranslationHelper {

  /**
   * Installs the configurable language for the given langcode.
   *
   * @param string $langcode
   *   The language code.
   * @param int $weight
   *   The language's weight. Defaults to 1.
   *
   * @return \Drupal\language\ConfigurableLanguageInterface
   *   The language entity.
   */
  public static function installLanguage(string $langcode, int $weight = 1) {
    $language_entity = ConfigurableLanguage::createFromLangcode($langcode);
    $language_entity->setWeight($weight);
    $language_entity->save();
    return $language_entity;
  }

  /**
   * Makes the given (bundle of an) content entity type translatable.
   *
   * @param string $entity_type
   *   The entity type name.
   * @param string $bundle
   *   The bundle type name.
   */
  public static function makeContentEntityTypeBundleTranslatable(string $entity_type, string $bundle) {
    $language_configuration = ContentLanguageSettings::loadByEntityTypeBundle($entity_type, $bundle);
    $language_configuration->setLanguageAlterable(TRUE);
    $language_configuration->setThirdPartySetting('content_translation', 'enabled', TRUE);
    $language_configuration->save();
  }

  /**
   * Translates a given locale string for the specified language.
   *
   * @param string $source
   *   The (english) source string.
   * @param string $target
   *   The translation in the target language.
   * @param string $langcode
   *   The language code of the target language. Defaults to 'de'.
   */
  public static function translateLocaleString(string $source, string $target, string $langcode = 'de') {
    /** @var \Drupal\locale\StringDatabaseStorage $locale_storage */
    $locale_storage = \Drupal::service('locale.storage');
    // Find existing source string.
    $string = $locale_storage->findString(['source' => $source]);
    if (empty($string)) {
      $string = new SourceString();
      $string->setString($source);
      $string->setStorage($locale_storage);
      $string->save();
    }
    // Create translation. If one already exists, it will be replaced.
    $locale_storage->createTranslation([
      'lid' => $string->lid,
      'language' => $langcode,
      'translation' => $target,
    ])->save();
  }

  /**
   * Deletes both source and translations for given locale string.
   *
   * @param string $source
   *   The (english) source string.
   */
  public static function deleteLocaleString(string $source) {
    /** @var \Drupal\locale\StringDatabaseStorage $locale_storage */
    $locale_storage = \Drupal::service('locale.storage');
    $locale_storage->deleteStrings(['source' => $source]);
  }

}
