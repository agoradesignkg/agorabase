<?php

namespace Drupal\agorabase;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Default block renderer implementation.
 */
class BlockRenderer implements BlockRendererInterface {

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The current active user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new BlockRenderer object.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current active user.
   */
  public function __construct(BlockManagerInterface $block_manager, AccountProxyInterface $current_user) {
    $this->blockManager = $block_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function renderBlock(string $block_plugin_id, array $block_config = []): array {
    /** @var \Drupal\Core\Block\BlockPluginInterface $block */
    $block = $this->blockManager->createInstance($block_plugin_id, $block_config);
    $build = [
      '#cache' => [
        'contexts' => $block->getCacheContexts(),
        'tags' => $block->getCacheTags(),
        'max_age' => $block->getCacheMaxAge(),
      ],
    ];
    // Return empty render array if user doesn't have access.
    if (!$block->access($this->currentUser)) {
      return $build;
    }
    $block_build = $block->build();
    $build += $block_build;
    return $build;
  }

}
