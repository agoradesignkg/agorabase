<?php

namespace Drupal\agorabase\Plugin\Filter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\Attribute\Filter;
use Drupal\filter\Plugin\FilterInterface;

/**
 * Provides a filter for converting Markdown-like line-breaks in HTML.
 *
 * Replaces '||' in given string with <br/> tags.
 *
 * This is not really Markdown syntax, but we'll keep the naming, as we started
 * with Markdown bold filter and now are enhancing this family.
 */
#[Filter(
  id: "markdown_linebreak",
  title: new TranslatableMarkup("Markdown line break"),
  type: FilterInterface::TYPE_MARKUP_LANGUAGE,
  description: new TranslatableMarkup("Allows content to use '||' pseudo syntax for line breaks that is filtered into valid HTML."),
  weight: -15
)]
class MarkdownLinebreak extends MarkdownConverterBase {

  /**
   * {@inheritdoc}
   */
  public static function getFilterId(): string {
    return 'markdown_linebreak';
  }

  /**
   * {@inheritdoc}
   */
  public static function getPseudoMarkup(): string {
    return '||';
  }

  /**
   * {@inheritdoc}
   */
  public function replacePseudoMarkup(string $text): string {
    if (!empty($text)) {
      $pseudo_markup = static::getPseudoMarkup();
      $text = str_replace($pseudo_markup, '<br/>', $text);
    }
    return $text;
  }

  /**
   * {@inheritdoc}
   */
  public function removePseudoMarkup(string $text): string {
    $pseudo_markup = static::getPseudoMarkup();
    return str_replace($pseudo_markup, ' ', $text);
  }

}
