<?php

namespace Drupal\agorabase\Plugin\Filter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\Attribute\Filter;
use Drupal\filter\Plugin\FilterInterface;

/**
 * Provides a filter for converting Markdown style superscript text in HTML.
 *
 * Replaces '^' in given string with <sup> tags.
 */
#[Filter(
  id: "markdown_superscript",
  title: new TranslatableMarkup("Markdown superscript"),
  type: FilterInterface::TYPE_MARKUP_LANGUAGE,
  description: new TranslatableMarkup("Allows content to use Markdown syntax for superscript (^) that is filtered into valid HTML."),
  weight: -15
)]
class MarkdownSuperscript extends MarkdownConverterBase {

  /**
   * {@inheritdoc}
   */
  public static function getFilterId(): string {
    return 'markdown_superscript';
  }

  /**
   * {@inheritdoc}
   */
  public static function getPseudoMarkup(): string {
    return '^';
  }

  /**
   * {@inheritdoc}
   */
  public function replacePseudoMarkup(string $text): string {
    if (!empty($text)) {
      $pseudo_markup = static::getPseudoMarkup();

      $num_pseudo_markup = substr_count($text, $pseudo_markup);
      // We do not have an equal number of this sign, so entirely skip it.
      if ($num_pseudo_markup % 2 !== 0) {
        return $text;
      }

      $count = 0;
      $pos = strpos($text, $pseudo_markup);
      while ($pos !== FALSE) {
        $count++;
        $replace = $count % 2 ? '<sup>' : '</sup>';
        $text = substr_replace($text, $replace, $pos, 1);
        $pos = strpos($text, $pseudo_markup);
      }
    }
    return $text;
  }

}
