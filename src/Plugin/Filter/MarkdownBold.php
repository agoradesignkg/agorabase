<?php

namespace Drupal\agorabase\Plugin\Filter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\Attribute\Filter;
use Drupal\filter\Plugin\FilterInterface;

/**
 * Provides a filter for converting Markdown style bold text in HTML.
 *
 * Replaces '**' in given string with <strong> tags.
 */
#[Filter(
  id: "markdown_bold",
  title: new TranslatableMarkup("Markdown bold"),
  type: FilterInterface::TYPE_MARKUP_LANGUAGE,
  description: new TranslatableMarkup("Allows content to use Markdown syntax for bold text (**) that is filtered into valid HTML."),
  weight: -15
)]
class MarkdownBold extends MarkdownConverterBase implements MarkdownBoldConverterInterface {

  /**
   * {@inheritdoc}
   */
  public static function getFilterId(): string {
    return 'markdown_bold';
  }

  /**
   * {@inheritdoc}
   */
  public static function getPseudoMarkup(): string {
    return '**';
  }

  /**
   * {@inheritdoc}
   */
  public function replacePseudoMarkup(string $text): string {
    if (!empty($text)) {
      $pseudo_markup = static::getPseudoMarkup();
      $count = 0;
      $pos = strpos($text, $pseudo_markup);
      while ($pos !== FALSE) {
        $count++;
        $replace = $count % 2 ? '<strong>' : '</strong>';
        $text = substr_replace($text, $replace, $pos, 2);
        $pos = strpos($text, $pseudo_markup);
      }
    }
    return $text;
  }

}
