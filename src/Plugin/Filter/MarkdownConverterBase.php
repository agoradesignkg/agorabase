<?php

namespace Drupal\agorabase\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Abstract Markdown converter base class.
 */
abstract class MarkdownConverterBase extends FilterBase implements MarkdownConverterInterface {

  /**
   * {@inheritdoc}
   */
  public static function getInstance() {
    $filter_id = static::getFilterId();
    $cache_id = __METHOD__ . '__' . $filter_id;
    $filter = &drupal_static($cache_id);
    if (!isset($filter)) {
      /** @var \Drupal\filter\FilterPluginManager $filter_plugin_manager */
      $filter_plugin_manager = \Drupal::service('plugin.manager.filter');
      /** @var \Drupal\agorabase\Plugin\Filter\MarkdownBoldConverterInterface $filter */
      $filter = $filter_plugin_manager->createInstance(static::getFilterId());
    }
    return $filter;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $text = $this->replacePseudoMarkup($text);
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function removePseudoMarkup(string $text): string {
    $pseudo_markup = static::getPseudoMarkup();
    return str_replace($pseudo_markup, '', $text);
  }

}
