<?php

namespace Drupal\agorabase\Plugin\Filter;

/**
 * Provides an interface for working with Markdown bold syntax.
 *
 * Replaces '**' in given string with <strong> tags.
 */
interface MarkdownBoldConverterInterface extends MarkdownConverterInterface {

}
