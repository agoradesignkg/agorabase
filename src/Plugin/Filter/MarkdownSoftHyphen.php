<?php

namespace Drupal\agorabase\Plugin\Filter;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\Attribute\Filter;
use Drupal\filter\Plugin\FilterInterface;

/**
 * Provides a filter for converting Markdown-like soft hyphens in HTML.
 *
 * Replaces '--' in given string with &shy; HTML.
 *
 * This is not really Markdown syntax, but we'll keep the naming, as we started
 * with Markdown bold filter and now are enhancing this family.
 */
#[Filter(
  id: "markdown_soft_hyphen",
  title: new TranslatableMarkup("Markdown soft hyphen"),
  type: FilterInterface::TYPE_MARKUP_LANGUAGE,
  description: new TranslatableMarkup("Allows content to use '--' pseudo syntax for soft hyphens that is filtered into valid HTML."),
  weight: -15
)]
class MarkdownSoftHyphen extends MarkdownConverterBase {

  /**
   * {@inheritdoc}
   */
  public static function getFilterId(): string {
    return 'markdown_soft_hyphen';
  }

  /**
   * {@inheritdoc}
   */
  public static function getPseudoMarkup(): string {
    return '--';
  }

  /**
   * {@inheritdoc}
   */
  public function replacePseudoMarkup(string $text): string {
    if (!empty($text)) {
      $pseudo_markup = static::getPseudoMarkup();
      $text = str_replace($pseudo_markup, '&shy;', $text);
    }
    return $text;
  }

}
