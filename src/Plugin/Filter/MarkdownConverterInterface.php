<?php

namespace Drupal\agorabase\Plugin\Filter;

/**
 * Provides an interface for simple Markdown-like filters.
 *
 * We keep the word "Markdown" in this common interface, as we started with the
 * bold syntax converter, which is using on Markup bold syntax. We'll however
 * introduce different implementations that are simple yet not Markdown syntax.
 */
interface MarkdownConverterInterface {

  /**
   * Returns a global markdown filter instance.
   *
   * @return \Drupal\agorabase\Plugin\Filter\MarkdownConverterInterface
   *   The markdown filter instance.
   */
  public static function getInstance();

  /**
   * Returns the filter ID, as defined via class annotations.
   *
   * @return string
   *   The filter ID. Must match the ID defined via class annotations.
   */
  public static function getFilterId(): string;

  /**
   * The pseudo markup that is replaced by this filter.
   *
   * @return string
   *   The pseudo markup that is replaced by this filter.
   */
  public static function getPseudoMarkup(): string;

  /**
   * Replaces pseudo markup with HTML tags.
   *
   * @param string $text
   *   The text to replace.
   *
   * @return string
   *   The replaced text.
   */
  public function replacePseudoMarkup(string $text): string;

  /**
   * Removes all occurrences of the pseudo markup from given string.
   *
   * @param string $text
   *   The text.
   *
   * @return string
   *   The changed text.
   */
  public function removePseudoMarkup(string $text): string;

}
