<?php

namespace Drupal\agorabase\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'labelled_string_default' widget.
 *
 * @FieldWidget(
 *   id = "labelled_string_default",
 *   label = @Translation("Labelled string"),
 *   field_types = {
 *     "labelled_string"
 *   },
 * )
 */
class LabelledStringDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->label ?? NULL,
      '#size' => 60,
      '#placeholder' => '',
      '#maxlength' => 255,
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];

    $element['value'] = [
      '#title' => $this->t('Value'),
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#size' => 60,
      '#placeholder' => '',
      '#maxlength' => 255,
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
    ];

    return $element;
  }

}
