<?php

namespace Drupal\agorabase\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'labelled_string' field type.
 *
 * @FieldType(
 *   id = "labelled_string",
 *   label = @Translation("Labelled string"),
 *   description = @Translation("A field containing a value consisting of two plain text strings - one for the label, one for the value."),
 *   category = @Translation("Text"),
 *   default_widget = "labelled_string_default",
 *   default_formatter = "labelled_string_default"
 * )
 */
class LabelledStringItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'label' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => FALSE,
        ],
        'value' => [
          'type' => 'varchar',
          'length' => 255,
          'binary' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['label'] = DataDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE);

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();

    return [
      'label' => $random->word(mt_rand(1, 255)),
      'value' => $random->word(mt_rand(1, 255)),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $label = $this->label;
    $value = $this->value;
    return $value === NULL || $value === '' || $label === NULL || $label === '';
  }

}
