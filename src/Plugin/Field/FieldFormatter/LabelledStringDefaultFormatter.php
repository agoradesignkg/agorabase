<?php

namespace Drupal\agorabase\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'labelled_string_default' formatter.
 *
 * @FieldFormatter(
 *   id = "labelled_string_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "labelled_string",
 *   },
 * )
 */
class LabelledStringDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'labelled_string',
        '#label' => $item->label,
        '#value' => $item->value,
      ];
    }

    return $element;
  }

}
