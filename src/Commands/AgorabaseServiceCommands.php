<?php

namespace Drupal\agorabase\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush service commands.
 */
class AgorabaseServiceCommands extends DrushCommands {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The file storage.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Constructs a new AgorabaseServiceCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * Cleanup (delete) unused images.
   *
   * @command agorabase:cleanup-images
   *
   * @usage agorabase:cleanup-images
   *   Cleanup (delete) unused images.
   *
   * @aliases agb:cui
   */
  public function cleanupUnusedImages() {
    $query = $this->database->select('file_managed', 'fm');
    $query->fields('fm');
    $subquery = $this->database->select('file_usage', 'fu');
    $subquery->fields('fu', ['fid']);
    $query->condition('fm.fid', $subquery, 'NOT IN');
    $result = $query->execute();
    $count = 0;
    foreach ($result as $row) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->fileStorage->load($row->fid);
      if ($file) {
        $file->delete();
        $count++;
        $this->output()->writeln(sprintf('deleted file ID %s (%s)', $row->fid, $row->uri));
      }
      else {
        $this->output()->writeln(sprintf('unable to load file ID %s (%s)', $row->fid, $row->uri));
      }
    }
    $this->output()->writeln(sprintf('Finished. Total number of deleted files: %s', $count));
  }

  /**
   * Creates a user account having the moderator role.
   *
   * @param string $username
   *   The user name. If left empty, the name will be generated based on the
   *   domain name of the given e-mail address.
   * @param array $options
   *   Drush command options.
   *
   * @command agorabase:create_moderator
   *
   * @option string|null email
   *   The email address. If left empty, the site mail address will be used.
   * @option string langcode
   *   The language code. Defaults to 'de'.
   * @option string|null password
   *   An optional password (unhashed). If left empty, a random one will be
   *   generated.
   * @option bool transfer-authorship
   *   Whether to transfer existing node's authorship to the new user. Defaults
   *   to FALSE.
   *
   * @usage agorabase:create_moderator johndoe --email=john.doe@example.com
   *   --password=1234 Creates an user account having the moderator role.
   *
   * @aliases agb:cmu
   */
  public function createModeratorUser(string $username = '', array $options = [
    'email' => NULL,
    'langcode' => 'de',
    'password' => NULL,
    'transfer-authorship' => FALSE,
  ]) {
    if (empty($username)) {
      if (empty($options['email'])) {
        $options['email'] = $this->configFactory->get('system.site')->get('mail');
      }
      $domain = substr($options['email'], strpos($options['email'], '@') + 1);
      $domain_parts = explode('.', $domain, 2);
      $username = $domain_parts[0];
    }
    $user = agorabase_create_moderator_user($username, $options['email'], $options['langcode'], $options['password']);
    $this->output()->writeln(sprintf('Created user with UID %s.', $user->id()));

    if ($options['transfer-authorship']) {
      /** @var \Drupal\node\NodeInterface $node */
      foreach ($this->nodeStorage->loadMultiple() as $node) {
        $node->setOwner($user)->save();
      }
    }
  }

  /**
   * Update the length of a text field which already contains data.
   *
   * Use with caution and backup database before running this command!
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $field_name
   *   The field name.
   * @param int $new_length
   *   The new field length.
   *
   * @command agorabase:change-textfield-maxlength
   *
   * @usage agorabase:change-textfield-maxlength node field_xyz 64
   *   Sets the max length of node's field_xyz to 64.
   */
  public function changeTextFieldMaxLength($entity_type_id, $field_name, $new_length) {
    $name = sprintf('field.storage.%s.%s', $entity_type_id, $field_name);
    $new_length = (int) $new_length;

    // Get the current settings.
    $result = $this->database->query(
      'SELECT data FROM {config} WHERE name = :name',
      [':name' => $name]
    )->fetchField();
    $data = unserialize($result);
    $data['settings']['max_length'] = $new_length;

    // Write settings back to the database.
    $this->database->update('config')
      ->fields(['data' => serialize($data)])
      ->condition('name', $name)
      ->execute();

    // Update the value col in both _data and _revision tables for the field.
    $table = $entity_type_id . "__" . $field_name;
    $table_revision = $entity_type_id . "_revision__" . $field_name;
    $new_field = ['type' => 'varchar', 'length' => $new_length];
    $col_name = $field_name . '_value';
    $this->database->schema()->changeField($table, $col_name, $col_name, $new_field);
    if ($this->database->schema()->tableExists($table_revision)) {
      $this->database->schema()
        ->changeField($table_revision, $col_name, $col_name, $new_field);
    }

    // Next, we also need to update the entity storage schema.
    $name = sprintf('%s.field_schema_data.%s', $entity_type_id, $field_name);
    $result = $this->database->query(
      'SELECT value FROM {key_value} WHERE collection = :collection AND name = :name',
      [
        ':collection' => 'entity.storage_schema.sql',
        ':name' => $name,
      ]
    )->fetchField();
    $sql_schema = unserialize($result);
    foreach ($sql_schema as &$table_schema) {
      $table_schema['fields'][$col_name]['length'] = $new_length;
    }
    $this->database->update('key_value')
      ->fields(['value' => serialize($sql_schema)])
      ->condition('collection', 'entity.storage_schema.sql')
      ->condition('name', $name)
      ->execute();

    // And we also have to update the entity.definitions.installed entry,
    // otherwise we would still face entity mismatch warnings.
    $result = $this->database->query(
      'SELECT value FROM {key_value} WHERE collection = :collection AND name = :name',
      [
        ':collection' => 'entity.definitions.installed',
        ':name' => $entity_type_id . '.field_storage_definitions',
      ]
    )->fetchField();
    $entity_definition_data = unserialize($result);
    /** @var \Drupal\field\FieldStorageConfigInterface $field_storage_config */
    $field_storage_config =& $entity_definition_data[$field_name];
    $field_storage_config->setSetting('max_length', $new_length);
    $this->database->update('key_value')
      ->fields(['value' => serialize($entity_definition_data)])
      ->condition('collection', 'entity.definitions.installed')
      ->condition('name', $entity_type_id . '.field_storage_definitions')
      ->execute();

    // Flush the caches.
    drupal_flush_all_caches();
  }

}
