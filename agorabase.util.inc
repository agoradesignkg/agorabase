<?php

/**
 * @file
 * Utility functions of the agorabase module.
 *
 * Include this file with the following command:
 * \Drupal::moduleHandler()->loadInclude('agorabase', 'inc', 'agorabase.util');
 */

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\link\LinkItemInterface;
use Drupal\user\RoleInterface;

/**
 * Overrides width (and height) of an image style.
 *
 * The first supported effect will be updated. Currently, the following effect
 * plugins are supported.
 *   - image_scale_and_crop
 *   - focal_point_scale_and_crop.
 *
 * @param string $image_style_name
 *   The image style name.
 * @param int $width
 *   The width in px.
 * @param int|null $height
 *   The optional height in px.
 */
function agorabase_override_image_style_scale(string $image_style_name, int $width, ?int $height): void {
  /** @var \Drupal\image\ImageStyleInterface|null $image_style */
  $image_style = ImageStyle::load($image_style_name);
  if (empty($image_style)) {
    \Drupal::logger('error')->warning('Image style @style does not exist.', ['@style' => $image_style_name]);
    return;
  }

  $allowed_plugins = [
    'image_scale',
    'image_scale_and_crop',
    'focal_point_scale_and_crop',
  ];

  $it = $image_style->getEffects()->getIterator();
  /** @var \Drupal\image\ImageEffectInterface $effect */
  foreach ($it as $effect) {
    if (in_array($effect->getPluginId(), $allowed_plugins)) {
      $config = $effect->getConfiguration();
      $config['data']['width'] = $width;
      if ($height) {
        $config['data']['height'] = $height;
      }
      $effect->setConfiguration($config);
      break;
    }
  }
  $image_style->save();
}

/**
 * Adds webp conversion to the given image style.
 *
 * There must not be a 'image_convert' effect inside already.
 *
 * @param string $image_style_name
 *   The image style name.
 */
function agorabase_add_image_style_webp_conversion(string $image_style_name): void {
  /** @var \Drupal\image\ImageStyleInterface|null $image_style */
  $image_style = ImageStyle::load($image_style_name);
  if (empty($image_style)) {
    \Drupal::logger('error')->warning('Image style @style does not exist.', ['@style' => $image_style_name]);
    return;
  }

  $max_weight = 0;
  $it = $image_style->getEffects()->getIterator();
  /** @var \Drupal\image\ImageEffectInterface $effect */
  foreach ($it as $effect) {
    if ($effect->getPluginId() === 'image_convert') {
      // There's already an image convert plugin inside.
      return;
    }
    $max_weight = max($max_weight, $effect->getWeight());
  }
  $max_weight++;

  $config = [
    'id' => 'image_convert',
    'weight' => $max_weight,
    'data' => [
      'extension' => 'webp',
    ],
  ];
  $image_style->addImageEffect($config);
  $image_style->save();
}

/**
 * Utility function migrating core scale and crop styles to focal_point ones.
 *
 * @param string $image_style_name
 *   The image style name.
 */
function agorabase_migrate_image_style_to_focal_point(string $image_style_name): void {
  /** @var \Drupal\image\ImageStyleInterface|null $image_style */
  $image_style = ImageStyle::load($image_style_name);
  if (empty($image_style)) {
    \Drupal::logger('error')->warning('Image style @style does not exist.', ['@style' => $image_style_name]);
    return;
  }

  $it = $image_style->getEffects()->getIterator();
  /** @var \Drupal\image\ImageEffectInterface $effect */
  foreach ($it as $effect) {
    if ($effect->getPluginId() === 'image_scale_and_crop') {
      $config = $effect->getConfiguration();
      $config['id'] = 'focal_point_scale_and_crop';
      $config['data']['crop_type'] = 'focal_point';
      unset($config['data']['anchor']);
      $image_style->deleteImageEffect($effect);
      $image_style->addImageEffect($config);
    }
  }
  $image_style->save();
}

/**
 * Utility function migrating focal_point scale and crop styles to core ones.
 *
 * @param string $image_style_name
 *   The image style name.
 */
function agorabase_migrate_image_style_to_core_scale_and_crop(string $image_style_name): void {
  /** @var \Drupal\image\ImageStyleInterface|null $image_style */
  $image_style = ImageStyle::load($image_style_name);
  if (empty($image_style)) {
    \Drupal::logger('error')->warning('Image style @style does not exist.', ['@style' => $image_style_name]);
    return;
  }

  $it = $image_style->getEffects()->getIterator();
  /** @var \Drupal\image\ImageEffectInterface $effect */
  foreach ($it as $effect) {
    if ($effect->getPluginId() === 'focal_point_scale_and_crop') {
      $config = $effect->getConfiguration();
      $config['id'] = 'image_scale_and_crop';
      $config['data']['anchor'] = 'center-center';
      unset($config['data']['crop_type']);
      $image_style->deleteImageEffect($effect);
      $image_style->addImageEffect($config);
    }
  }
  $image_style->save();
}

/**
 * Creates the "field_readmore_link" field for the given entity type and bundle.
 *
 * @param string $entity_type_id
 *   The entity type ID.
 * @param string $bundle
 *   The bundle name. Please note that the entity type and bundle is not checked
 *   for existence!
 * @param int $form_display_weight
 *   The sort weight to be used in the form display settings. Defaults to 3.
 * @param int $view_display_weight
 *   The sort weight to be used in the form display settings. Defaults to 3.
 * @param string $label
 *   The field label. Defaults to 'Readmore Link'.
 */
function agorabase_create_readmore_field(string $entity_type_id, string $bundle, int $form_display_weight = 3, int $view_display_weight = 3, string $label = 'Readmore Link'): void {
  $field_name = 'field_readmore_link';
  $field_storage = agorabase_get_or_create_readmore_field_storage($entity_type_id);

  $field = FieldConfig::create([
    'field_storage' => $field_storage,
    'bundle' => $bundle,
    'label' => $label,
    'settings' => [
      'title' => 1,
      'link_type' => LinkItemInterface::LINK_GENERIC,
    ],
  ]);
  $field->save();

  /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
  $display_repository = \Drupal::service('entity_display.repository');

  // Assign widget settings for the default form mode.
  $display_repository->getFormDisplay($entity_type_id, $bundle)
    ->setComponent($field_name, [
      'type' => 'link_default',
      'weight' => $form_display_weight,
    ])
    ->save();

  // Assign display settings for the 'default' view mode.
  $display_repository->getViewDisplay($entity_type_id, $bundle)
    ->setComponent($field_name, [
      'label' => 'hidden',
      'type' => 'link',
      'weight' => $view_display_weight,
      'settings' => [
        'trim_length' => 80,
        'url_only' => FALSE,
        'url_plain' => FALSE,
        'rel' => '',
        'target' => '',
      ],
    ])
    ->save();
}

/**
 * Get or create field_readmore_link field storage for the given entity type.
 *
 * Will be created and saved automatically, if not existing.
 *
 * @param string $entity_type_id
 *   The entity type ID.
 *
 * @return \Drupal\field\FieldStorageConfigInterface
 *   The field_readmore_link field storage for the given entity type.
 */
function agorabase_get_or_create_readmore_field_storage(string $entity_type_id): FieldStorageConfigInterface {
  $field_name = 'field_readmore_link';
  /** @var \Drupal\field\FieldStorageConfigInterface|null $field_storage */
  $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);

  if (!$field_storage) {
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type_id,
      'type' => 'link',
    ]);
    $field_storage->setCardinality(1);
    $field_storage->setTranslatable(TRUE);
    $field_storage->save();
  }

  return $field_storage;
}

/**
 * Sets default permissions for the given taxonomy vocabulary ID.
 *
 * @param string $vocabulary_id
 *   The taxonomy vocabulary ID.
 * @param string|null $moderator_role
 *   The optional moderator role ID. If left NULL, agorabase_get_moderator_role
 *   function will be called to determine the moderator role. Defaults to NULL.
 */
function agorabase_set_default_taxonomy_permissions(string $vocabulary_id, ?string $moderator_role = NULL): void {
  $view_permission = 'view terms in ' . $vocabulary_id;
  user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, [
    $view_permission,
  ]);
  user_role_grant_permissions(RoleInterface::AUTHENTICATED_ID, [
    $view_permission,
  ]);
  if ($moderator_role = $moderator_role ?: agorabase_get_moderator_role()) {
    user_role_grant_permissions($moderator_role, [
      'create terms in ' . $vocabulary_id,
      'delete terms in ' . $vocabulary_id,
      'edit terms in ' . $vocabulary_id,
      'reorder terms in ' . $vocabulary_id,
    ]);
  }
}

/**
 * Create default taxonomy terms.
 *
 * @param string $vocabulary_id
 *   The taxonomy vocabulary ID.
 * @param string[] $tags
 *   The default tags.
 */
function agorabase_create_default_terms(string $vocabulary_id, array $tags): void {
  $term_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
  foreach ($tags as $delta => $tag) {
    $term = $term_storage->create([
      'vid' => $vocabulary_id,
      'name' => $tag,
      'status' => 1,
      'weight' => $delta,
    ]);
    $term->save();
  }
}
